#!/bin/bash

test -e $1 && echo ok || exit 1
test -e $2 && echo ok || exit 1

x=$(wc -l < $1)
y=$(wc -l < $2)

test $x -gt $y && echo $1 || echo $2
